import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { BackgroundGeolocation } from 'ionic-native';
//import { LocationTracker } from '../providers/location-tracker';
import { Storage } from '@ionic/storage';
import { Data } from '../providers/dataStore';
import { StateCityService } from '../providers/statecity';
import { GlobalVars } from '../providers/global';
import { LocationTracker } from '../providers/locationTracker';

import { ConnectivityService } from '../providers/connectivityService';
import { MyApp } from './app.component';
import { Login } from '../pages/login/login';
import { Home } from '../pages/home/home';
import { Upload } from '../pages/upload/upload';
import { Popover } from '../pages/popover/popover';
import { regList } from '../pages/regList/regList';
import { regForm } from '../pages/regForm/regForm';
import { Site } from '../pages/site/site';
import { documentUpload } from '../pages/documentUpload/documentUpload';
import { Settings } from '../pages/settings/settings';

//import { SERVERNAME } from '../config';

@NgModule({
    declarations: [ MyApp, Login, Home, regList, regForm, Popover, Site, Upload, documentUpload, Settings ],
imports: [
IonicModule.forRoot(MyApp),HttpModule
],
bootstrap: [IonicApp],
entryComponents: [ MyApp, Login, Home, regList, regForm, Popover, Site, Upload, documentUpload, Settings ],
    providers: [ Storage, Data, StateCityService, ConnectivityService, BackgroundGeolocation, GlobalVars, LocationTracker, {provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {

}
