import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ToastController, LoadingController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Login } from '../pages/login/login';
import { Home } from '../pages/home/home';
import { Upload } from '../pages/upload/upload';
import { regList } from '../pages/regList/regList';
import { Settings } from '../pages/settings/settings';
import { documentUpload } from '../pages/documentUpload/documentUpload';
import { Data } from '../providers/dataStore';
import { ConnectivityService } from '../providers/connectivityService';
import { LocationTracker } from '../providers/locationTracker';
import { GlobalVars } from '../providers/global';

@Component({  
  templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;
    rootPage: any;
    pages: Array<{icon: string, title: string, component: any}>;
    savedUser:any = {};
    public loader:any;
    constructor(public platform: Platform, public dataService: Data, public locationTracker:LocationTracker, public connectivityservice: ConnectivityService, public global : GlobalVars, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
        //{ title: 'Page One', component: Page1 },
            { icon:'home', title: 'Home', component: Home },
            { icon:'md-list', title: 'List', component: regList },
            { icon:'md-sync', title: 'Upload', component: Upload },
            { icon:'md-document', title: 'Document', component: documentUpload },
            { icon:'md-settings', title: 'Settings', component: Settings }
        ];
    }
    ngOnInit(){ 
        
        this.dataService.getUser().then((data)=>{
            if(data!=null)
            {                
                this.savedUser = JSON.parse(data);                
                if(this.savedUser.status == true){                    
                    this.nav.setRoot(regList);
                }
                else{
                    this.rootPage = Login;                 
                }
                
            }
            else{ console.log('Login page...');
                this.rootPage = Login;                 
             }            
        });      
    }
    ionViewWillEnter() {
        //this.locationTracker.startTracking();
        
    }    
    initializeApp() {
      this.platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        StatusBar.styleDefault();
        Splashscreen.hide();        
        if(this.connectivityservice.isOnline())
        {
            this.dataService.getLocation().then((data)=>{ 
                console.log(this.global.trackedLocation);
                console.log('Data here.. ',data, data.length);                
                if(data!=null && data.length>0)
                {                                                          
                    /*Post data to server now. once posted clear localstore & global Variable.*/                    
                    this.dataService.synLocation(data).subscribe(
                    result =>{
                        console.log('Location data', result);
                        if(result.status)
                        {
                            this.showToast('Location data uploaded.');
                            this.global.trackedLocation = []; /*if data sync success, make it empty.*/
                            this.dataService.setLocation([]);                            
                            this.locationTracker.deleteAllLocations();                            
                        }
                        else{
                            this.showToast('failed to upload location data.');
                        }
                    },
                    error =>{ this.showToast('Fail to Upload'+ error); console.log(error); },
                    () => { console.log('Done...'); })                    
                }
                else{
                    this.showToast('Location tracking data is empty.');
                }
            });
        }        
        //this.locationTracker.startTracking();
      });
    }

    openPage(page) 
    {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
      this.nav.setRoot(page.component);
    }
    logout(){
        this.savedUser.status = false;
        this.dataService.setUser(this.savedUser);
        this.locationTracker.stopTracking();
        this.nav.setRoot(Login);
        console.log('Logout');
    }    
    showToast(value){
         let toast = this.toastCtrl.create({
            message: value,
            duration: 5000            
        });
        toast.present();
    }
}
