import { Storage } from '@ionic/storage';
import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class Data{
	//public SERVERNAME: string = 'http://www.infraprix.com'; /*PRODUCTION*/
	public SERVERNAME: string = 'http://35.154.136.252'; /* STAGING */
	//public SERVERNAME: string = 'http://uat.infraprix.com'; /* UAT */
	//public SERVERNAME: string = 'http://192.168.1.119:5000'; /* Local */

	public syncDataList = [];
	public savedUser:any = {};
	//.constant("configParamUrl", "http://www.infraprix.com") // Production
	//.constant("configParamUrl", "http://35.154.136.252")  // staging
	constructor(public storage: Storage, public http: Http)
	{

	}
	getLocal(){
		//return ( this.storage.get('localList')!=null ? this.storage.get('localList') : []);
		return this.storage.get('local');
	}
	setLocal(data){
		let formatToJson = JSON.stringify(data);
		this.storage.set('local', formatToJson);
	}
	clearLocal(){
		this.storage.remove('local');
	}
	getLive(){
		//return ( this.storage.get('localList')!=null ? this.storage.get('localList') : []);
		return this.storage.get('live');
	}
	setLive(data){
		let formatToJson = JSON.stringify(data);
		this.storage.set('live', formatToJson);
	}
	clearLive(){
		this.storage.remove('live');
	}
	getUser(){		
		return this.storage.get('user');
	}
	setUser(data){
		let formatToJson = JSON.stringify(data);
		this.storage.set('user', formatToJson);
	}
	clearUser(){
		this.storage.remove('user');
	}
	getLocation(){
		//return ( this.storage.get('location')!=null ? this.storage.get('location') : []);
		return this.storage.get('location');
	}
	setLocation(data){
		let formatToJson = JSON.stringify(data);
		this.storage.set('location', formatToJson);
	}
	getList(count, userid){		
        return this.http.post(this.SERVERNAME+'/bulker/getusers', JSON.stringify({ 'userId':userid, 'count':count }))
        .map(res => {        	
        	let data:any = res.json();        	
        	return data;
        });
    };
    synData(data, userid)
    {    	       	
    	return this.http.post(this.SERVERNAME+'/bulker/bulkregister', JSON.stringify({ 'bulkerArray': data, 'userId':userid }))		    	
		.map(res => {		        	
        	return res;
        });		    	
    }
    synLocation(data)
    {    	       	
    	let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        let options = new RequestOptions({ headers: headers});            
        let body = 'userlocation='+data;
    	return this.http.post('http://35.154.139.104/crm/apis/getUserLocation.php', body, options)
		.map(res => {		        	
        	return res;
        });		    	
    }
    
}