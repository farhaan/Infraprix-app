import { BackgroundGeolocation, Toast } from 'ionic-native';
import { ToastController } from 'ionic-angular';
import { GlobalVars } from './global';
import { Data } from './dataStore';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LocationTracker {
	public savedUser:any = {};
	constructor(public http: Http, public global : GlobalVars, public dataService: Data, public toastCtrl: ToastController) {		
	}
	ngOnInit(){			
        this.dataService.getUser().then((data)=>{
			if(data!=null)
			{ this.savedUser = JSON.parse(data); }			
		});
	}
	startTracking(userid)
	{       						
	    let config = {
	        desiredAccuracy: 100,
	        stationaryRadius: 150,
	        distanceFilter: 200,
	        //url: 'http://192.168.1.134:3000/locations',
	        locationProvider: BackgroundGeolocation.LocationProvider.ANDROID_ACTIVITY_PROVIDER,
	        interval: 900000,
	        stopOnTerminate: false
	    }
	    //BackgroundGeolocation.configure((Location)=>{}, (err)=>{}, config);
	    BackgroundGeolocation.configure((location) => {
	    		let userId = this.savedUser.userid || userid;
	    		let DD = new Date().getDate(); let MM = new Date().getMonth()+1; let YYYY = new Date().getFullYear();
	    		let time = new Date().getTime();
	    		let date = YYYY+'-'+MM+'-'+DD;	           
	            this.global.trackedLocation.push({ 'lat':location.latitude, 'long':location.longitude, 'date':date, 'time':time, 'userid':userId, 'status':1 });	            
	            console.log(this.global.trackedLocation);
                this.dataService.setLocation(this.global.trackedLocation);
	            Toast.show(location.latitude+" : "+location.longitude+" : "+date+" : "+time+":"+ userId, '5000', 'center').subscribe( toast => { console.log(toast); });                    
	        }, 
	        (error) => { this.showToast(error); },config);
	        
	    BackgroundGeolocation.watchLocationMode().then((enabled)=>{
	        if(enabled){	            
	            BackgroundGeolocation.start();
	        }
	        else{
	            BackgroundGeolocation.stop();
	            BackgroundGeolocation.stopWatchingLocationMode();	            
	        }
	    })
	    .catch((error)=>{ this.showToast(error); });

	    BackgroundGeolocation.isLocationEnabled().then((enabled)=>{	        
	        if(enabled){
	            BackgroundGeolocation.start();
	        }
	        else{	            
	            BackgroundGeolocation.showLocationSettings();
	        }                
	    }
	    ).catch((error)=>{ this.showToast(error); });
	}
	stopTracking(){
		BackgroundGeolocation.stop();
	}
	deleteAllLocations(){
		BackgroundGeolocation.deleteAllLocations();
	}
	showToast(value){
	     let toast = this.toastCtrl.create({
	        message: value,
	        duration: 5000            
	    });
	    toast.present();
	}
}
