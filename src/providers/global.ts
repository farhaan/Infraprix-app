import {Injectable} from '@angular/core';

@Injectable()
export class GlobalVars {
	public genabletracking:boolean = false;
	public trackedLocation = [];
	constructor() { }
	setEnableTracking(value) {
		this.genabletracking = value;
	}
	getEnableTracking() {
		return this.genabletracking;
	}
}