import {Injectable} from "@angular/core";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class StateCityService {

    constructor(public http: Http)
    {
    }
    getData(){
        return this.http.get('assets/data/statecity.json').map(res => res.json().data);
    };
    

}

// this.http.get('assets/data/statecity.json').map(res => res.json()).subscribe(data => {
//     this.statecity = data.data.children;
// });
