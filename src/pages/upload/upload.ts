import { Component } from '@angular/core';
import { LoadingController, ToastController, AlertController } from 'ionic-angular';
//import { Network } from 'ionic-native';

import { Data } from '../../providers/dataStore';
import { ConnectivityService } from '../../providers/connectivityService';

@Component({
    selector : 'upload',    
    templateUrl : 'upload.html'    
})
export class Upload {
	public uploadLocalData = [];
	public savedUser:any = {};
	public loader:any;
	public disconnectSubscription;
	public connectSubscription;
	constructor( public dataService: Data, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public alertCtrl: AlertController, public connectivityservice: ConnectivityService ){

	}
	ngOnInit(){
		
	}
	ionViewWillEnter(){
		this.dataService.getUser().then((data)=>{
            if(data!=null)
            {
                this.savedUser = JSON.parse(data);
                console.log(this.savedUser);                                
            }            
        });
        this.dataService.getLocal().then((data) => { 
            if(data)
            {                            	
                this.uploadLocalData = JSON.parse(data);
                console.log(this.uploadLocalData);                		       
            }            
        });
	}
	uploadData(){		
		if(this.connectivityservice.isOnline())
		{			
			if(this.uploadLocalData.length>0)
			{							
				this.showLoader();
				this.dataService.synData(this.uploadLocalData, this.savedUser.userid).subscribe(
			        data =>{ 		        	
			        	let res = data.json();		            			        	
			        	switch (res.statusCode) 
			        	{
			        		case "300":		        					        			
			        			let timeInterval = 3000;			        			
				        		for(let i in res.records){				        							        			
				        			this.showToastMobile('Fail to upload! already exist! '+res.records[i].name+' '+ (typeof res.records[i].mobile=='undefined' ? res.records[i].email:res.records[i].mobile) , timeInterval);				        								        			
				        			timeInterval = timeInterval + 3000;
				        			/*for( let j in this.uploadLocalData){

				        				if( typeof res.records[i].email !== 'undefined' && this.uploadLocalData[j].email !== res.records[i].email )
				        				{ 
				        					this.uploadLocalData.splice(Number(j),1); 
				        				}
				        				if( typeof res.records[i].mobile !== 'undefined' && this.uploadLocalData[j].mobile !== res.records[i].mobile)
				        				{ 
				        					this.uploadLocalData.splice(Number(j),1); 
				        				}				        								        				
				        			}*/
				        		};
				        		this.dataService.setLocal(this.uploadLocalData);
			        			break;
			        		case "301":
			        				this.showToast(res.error);
			        				break;
			        		case "200":
				        			this.showToast('Record uploaded...');
				        			this.dataService.setLocal([]);
				        			this.uploadLocalData = [];
				        			this.dataService.getList('0', this.savedUser.userid).subscribe(
	            					data =>{
	            						console.log(data.data);	            						
	            						switch (data.statusCode) {
	            							case "301":
	            								this.showAlert(data.statusCode, data.error);
	            								break;
	            							case "300":
	            								this.showAlert(data.statusCode, data.error);
	            								break;
	            							case "200":
	            								let resData = [];
	            								for(let i of data.data)        
						                        {                    
						                            i.exist = true;
						                            i.localID = this.makeid();
						                            resData.push(i);                    
						                        }
						                        this.dataService.setLive([]); 
	                    						this.dataService.setLive(resData);
	            								break;
	            							default:
	            								this.showAlert('Error', 'server error');	            								
												break;
	            						}
	            					},
	            					error =>{ this.showToast(error); console.log(error); },
						            () => {}
	            					);
				        			break;
			        		default:
				        			this.showToast(res.error);
				        			break;
			        	}
			        },
			        error =>{ this.showToast('Fail to Upload'+ error); this.loader.dismiss(); console.log(error);},
			        () => {		        		
			        		this.loader.dismiss(); console.log('Done...'); 
			        	}
			    );
			}
			else{ this.showToast('No new record/update to upload.'); }
		}
		else
		{
			this.showToast('Check network connection.');
		}		
		
	}
	showAlert(errorCode, error){
		let alert = this.alertCtrl.create({
		    title:'Error '+ errorCode,
		    message: error,
		    buttons: ['Dismiss']
		});
		alert.present();
	}
	showToast(value){
		 let toast = this.toastCtrl.create({
			message: value,
			duration: 5000			
		});
		toast.present();
	}
	showToastMobile(value, time){
		let toast = this.toastCtrl.create({
			message: value,
			duration: time			
		});
		toast.present();	
	}
	showLoader(){
		this.loader = this.loadingCtrl.create({
			content:'Please wait...',
			duration:15000
		});
		this.loader.present();
	}
	makeid()
    {
       var text = "";
       var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
       for( let i=0; i < 5; i++ )
           text += possible.charAt(Math.floor(Math.random() * possible.length));

       return text;
    }

}