import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup  } from '@angular/forms';
import { NavController, NavParams, Events } from 'ionic-angular';
//HostListener import { NativeStorage } from 'ionic-native';
import { StateCityService } from '../../providers/statecity';
import { Data } from '../../providers/dataStore';

@Component({
    selector : 'regForm',    
    templateUrl : 'regForm.html',        
})
export class regForm{
        
    public localData = [];
    public liveData = [];
    stateList:any;
    cityList:any;
    savedUser:any = {};
    registrationForm: FormGroup;
    category:any;
    regUser:any = {};
    listOfCategories: [{id: string, text: string}];
    userBusinessType: any;
    update: boolean = false;
    ManagerSeller:any;
    ManagerBuyer:any;
    constructor(public navCtrl:NavController, public navParams:NavParams, public events: Events, public formBuilder: FormBuilder, public stc: StateCityService, public dataService: Data)
    {        
        this.registrationForm = this.formBuilder.group(
        {
            companyName: ['', Validators.required],
            userType : ['', Validators.required],
            type: ['', Validators.required],
            category: [''],
            pan: ['', Validators.compose([Validators.required, Validators.pattern('([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}')])],
            tin: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{11}')])],
            password: [''],
            email: ['', Validators.compose([ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')])],
            mobile: ['', Validators.compose([ Validators.required, Validators.pattern('[0-9]{10}')])],
            state: ['', Validators.required],
            city: ['', Validators.required],
            address: ['', Validators.compose([ Validators.required, Validators.minLength(5)]) ]
            //pan: ['', Validators.compose([ Validators.required, Validators.pattern('([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}')])],
        });
        //this.items = [];
        this.listOfCategories = [{ 'id': '643', 'text': 'Cement'},
                                 { 'id': '648', 'text': 'CP fitings'},
                                 { 'id': '925', 'text': 'Paints'},
                                 { 'id': '647', 'text': 'Sanitary Ware'},
                                 { 'id': '644', 'text': 'Steel'},
                                 { 'id': '646', 'text': 'Stone'},
                                 { 'id': '645', 'text': 'Tiles'},
                                 { 'id': '649', 'text': 'Wood'}];
        //console.log( typeof this.listOfCategories[Symbol.iterator]);
    }
    /*@HostListener( 'keydown', ['$event'] )
    keyEvent( e )
    {
       var code = e.keyCode || e.which;
       console.log( "HostListener.keyEvent() - code=" + code );
       if( code === 13 )
       {
            console.log('Enter is pressed'+e);               
       }
    };*/
    /*validateSpace(){
        console.log('Validate Spcae');
        return true;
    }*/
    ngOnInit(){                
        this.stc.getData().subscribe(data =>{
            this.stateList = data;            
        });
        this.dataService.getLocal().then((data) => { 
            if(data)
            {                
                this.localData = JSON.parse(data);                
            }            
        });
        this.dataService.getLive().then((data) => { 
            if(data)
            {                
                this.liveData = JSON.parse(data);                
            }            
        });
        this.dataService.getUser().then((data)=>{
            if(data!=null)
            {
                this.savedUser = JSON.parse(data);
                if(this.savedUser.rollid=='H9')
                {
                    this.ManagerSeller=true;
                    this.regUser.userType = 'buyer';
                    this.userBusinessType = [{'text':'Contractor'},{'text':'Developer'}];                    
                }
                if(this.savedUser.rollid=='H8')
                {
                    this.ManagerBuyer=true;
                    this.regUser.userType = 'seller';  
                    this.userBusinessType = [{'text':'Dealer/Agency'}, {'text':'Distributor'}, {'text':'Supplier'}, {'text':'Trader'}, {'text':'Manufacturer'}];                  
                }                                
            }            
        }); 
    }
    ionViewDidLoad(){       
        if(typeof this.navParams.get('item')!=='undefined')
        {                        
            if(typeof this.navParams.get('item').exist == 'undefined')
            {   
                this.registrationForm.controls['password'].setValidators(Validators.required);
                this.registrationForm.controls['password'].updateValueAndValidity();             
            }
            else{
                this.registrationForm.controls['password'].setValidators(null);
                this.registrationForm.controls['password'].updateValueAndValidity();
            }
            this.regUser =  this.navParams.get('item');
            this.category =  this.navParams.get('formatCategory');            
            this.cityList = this.navParams.get('cities');

            this.setCategory(this.regUser.userType, false);            
            this.update = true;            
        }
        else{            
            this.registrationForm.controls['password'].setValidators(Validators.required);
            this.registrationForm.controls['password'].updateValueAndValidity();
        }               
    }
    setCategory(eventValue, value){ 
        console.log(eventValue, value);       
        if(value)
        {
            this.regUser.type="";
        }
        if(eventValue=='buyer')
        {                       
            this.userBusinessType = [{'text':'Contractor'},{'text':'Developer'}];
            this.registrationForm.controls['pan'].setValidators(null);
            this.registrationForm.controls['tin'].setValidators(null);
            this.registrationForm.controls['pan'].updateValueAndValidity();
            this.registrationForm.controls['tin'].updateValueAndValidity();
        }
        else{  
            this.registrationForm.controls['category'].setValidators(Validators.required);
            this.registrationForm.controls['pan'].setValidators(Validators.compose([Validators.required, Validators.pattern('([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}')]));
            this.registrationForm.controls['tin'].setValidators(Validators.compose([Validators.required, Validators.pattern('[0-9]{11}')]));
            this.registrationForm.controls['pan'].updateValueAndValidity();
            this.registrationForm.controls['tin'].updateValueAndValidity();
            this.userBusinessType = [{'text':'Dealer/Agency'}, {'text':'Distributor'}, {'text':'Supplier'}, {'text':'Trader'}, {'text':'Manufacturer'}];
        }
        //console.log(this.registrationForm.controls);        
    }
    sellerType(eventValue){           
        this.category = "";
    }
    formatListOfCategories(eventValue)
    {         
        if(typeof eventValue=='object')       
        {
            let format:any= eventValue;
            for(let i in format){
                if(i==='0')
                {                
                    this.regUser.categories = format[i].toString();
                }
                else{                
                    this.regUser.categories = this.regUser.categories+' |##| '+format[i].toString();
                }            
            }
        }
        else{
            this.regUser.categories = eventValue;
        }               
    }
    onStateChange(event)
    {                
        for(let i in this.stateList)
        {
            //console.log($scope.stateList[i].id);
            if(this.stateList[i].id == event)
            {
                this.regUser.stateId = this.stateList[i].id;
                this.regUser.state = this.stateList[i].statename;
                this.cityList = this.stateList[i].cities;                
                //console.log( typeof this.cityList[Symbol.iterator]);
                break;
            }
        }
    };
    onCityChange(eventValue){        
        for(let i in this.cityList){
            if(this.cityList[i].id==eventValue)
            {
                this.regUser.cityId = this.cityList[i].id;
                this.regUser.city = this.cityList[i].name;
            }
        }
    };
    searchLocal(data)
    {        
        for(let i in this.localData)
        {                       
            if(this.localData[i].localID == data.localID)
            {
                console.log('searchLocal',this.localData[i]);
                this.localData[i]= data;
                return true;
            }            
        }
        return false;
    }
    searchLive(data)
    {
        for(let i in this.liveData)
        {            
            if(this.liveData[i].localID == data.localID && this.liveData[i].exist)
            {                
                this.liveData[i]= data;
                console.log('searchLive',this.liveData[i]);
                return true;
            }            
        }
        return false;
    }   

    logForm(){            
        if(this.registrationForm.valid)        
        {
            if(this.update)                
            {         
                console.log(this.regUser);
                if(this.searchLocal(this.regUser))
                {                
                    for(let i in this.liveData)
                    {                    
                        if(this.liveData[i].localID == this.regUser.localID)
                        { 
                            this.liveData[i]= this.regUser; 
                        }                                
                    }                
                    this.dataService.setLocal(this.localData);
                    this.dataService.setLive(this.liveData);    
                }
                else{                
                    if(this.searchLive(this.regUser)){                    
                        this.regUser.isChanged = 1;                
                        this.localData.unshift(this.regUser);                    
                        this.dataService.setLive(this.liveData);    
                        this.dataService.setLocal(this.localData);
                    }                                
                }
            }        
            else
            {
                this.regUser.localID = this.makeid();
                this.regUser.isChanged = 0;
                this.regUser.listOfSites = [];
                this.localData.unshift(this.regUser);
                this.liveData.unshift(this.regUser);
                this.dataService.setLocal(this.localData);
                this.dataService.setLive(this.liveData);    
            }        
            this.events.publish('reloadList');
            this.navCtrl.pop(); 
        }       
    }
    ionViewWillLeave(){                
        this.events.publish('reloadListSite');        
    }
    makeid()
    {
       var text = "";
       var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
       for( let i=0; i < 5; i++ )
           text += possible.charAt(Math.floor(Math.random() * possible.length));
       return text;
    }

}

