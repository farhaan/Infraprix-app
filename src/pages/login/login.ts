import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup  } from '@angular/forms';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { regList } from '../regList/regList';
import { Data } from '../../providers/dataStore';
import { ConnectivityService } from '../../providers/connectivityService';
import { LocationTracker } from '../../providers/locationTracker';
/*import { GlobalVars } from '../../providers/global';*/
import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class Login {
	//@ViewChild(Nav) nav: Nav;
	loginForm: FormGroup;
	user:any = {};
	savedUser:any = {};
	public loader:any;
	constructor( public navCtrl: NavController, public formBuilder: FormBuilder, public http: Http, public locationTracker:LocationTracker, public connectivityservice: ConnectivityService, public dataService: Data, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
		console.log('constructor');
		this.loginForm = this.formBuilder.group(
        {
            username: ['', Validators.required],
            password : ['', Validators.required],                        
        });			
        			
	}
	ngOnInit(){	
		this.dataService.getUser().then((data)=>{
			if(data!=null)
			{
				this.savedUser = JSON.parse(data);									
			}			
		});
		console.log('Login ionViewWillEnter.');
	}
	ionViewWillEnter(){}
	submit(){		
		// http://35.154.139.104/crm CRM STAGING
		// http://crmuat.infraprix.com/ CRM UAT
		// http://35.154.139.104/crm/apis/Authenticate.php
		// http://192.168.1.169/infraprix/apis//Authenticate.php
		if(this.loginForm.valid)
		{
			if(this.connectivityservice.isOnline())
	        {
				this.showLoader();			
				let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
				let options = new RequestOptions({ headers: headers});			
				let body = 'username='+this.user.username+'&password='+this.user.password;
				
				this.http.post('http://35.154.139.104/crm/apis/Authenticate.php', body, options)
				.map(res => res.json())
				.subscribe((data)=> {
					if(data.status == 401)
					{
						console.log(data.message);
						this.showToast(data.message);
						this.loader.dismiss();				
					}
					else
					{
						if(this.savedUser.userid!=='undefined' && data.userId !== this.savedUser.userid)
						{						
							this.dataService.clearUser();
							this.dataService.setLocal([]);
							this.dataService.setLive([]);
						}
						let loginUser:any = {};
						loginUser.username = this.user.username;
						loginUser.sessionid = data.sessionId;
						loginUser.userid = data.userId;
						loginUser.rollid = data.role;
						loginUser.status = true;								
						this.dataService.setUser(loginUser);
						this.locationTracker.startTracking(loginUser.userid);
						this.loader.dismiss();
						
						this.navCtrl.setRoot(regList);				
					}			
				}, err => {console.error(err)},()=>{});
			}
			else{
				this.showToast('Check network connection.');
			}
		}
	}
	showLoader(){
		this.loader = this.loadingCtrl.create({
			content:'Please wait...',
			duration:10000
		});
		this.loader.present();
	}
	showToast(msg){
         let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000            
        });
        toast.present();
    }
}