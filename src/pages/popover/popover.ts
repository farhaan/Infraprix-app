import { Component,  } from '@angular/core';
import { NavParams, NavController, ViewController } from 'ionic-angular';
import { Site } from '../site/site';

@Component({
  template: `<ion-list>              
              <button ion-item (click)="site()"><ion-icon name='globe' item-left></ion-icon>Site</button>              
            </ion-list>`
})
export class Popover {
    public data;
    constructor(public viewCtrl: ViewController, public navCtrl: NavController, public params: NavParams) {        
        this.data = this.params.get('item');
    }
    close() {         
        this.viewCtrl.dismiss();
    }
    site()
    {
        
        let categories;
        this.data.userType = this.data.userType.toLowerCase();        
        if(this.data.userType == 'seller'){
            if(this.data.type!='Manufacturer')
            {
              categories = this.data.categories.split(' |##| ');      
            }
            else{
              categories = this.data.categories;  
            }
        }        
        this.navCtrl.push(Site, { item: this.data , formatCategory: categories });
        this.viewCtrl.dismiss();
    }
}
/*<button ion-item (click)="close()">Documentation</button>
<button ion-item (click)="close()">Showcase</button>
<button ion-item (click)="close()">GitHub Repo</button>*/