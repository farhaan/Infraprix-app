import { Component } from '@angular/core';

import { Validators, FormBuilder, FormGroup  } from '@angular/forms';
import { FileChooser, FilePath, Transfer } from 'ionic-native';
import { ToastController } from 'ionic-angular';
import { Data } from '../../providers/dataStore';

@Component({
  selector: 'documentUpload',
  templateUrl: 'documentUpload.html',
})
export class documentUpload {
  
  	documentForm: FormGroup;
  	public params:any = {};
  	public nativeFilepath:string;
  	public filename:string;
  	public savedUser:any = {};
  	public percentage:number = 0;
  	public showProgressBar:boolean = false;
    constructor( public formBuilder: FormBuilder, public toastCtrl: ToastController, public dataService: Data) {     	
    	this.documentForm = this.formBuilder.group(
        {
            type: ['', Validators.required],
            title : ['', Validators.required],
            description: ['']            
        });
    }
    ngOnInit(){
		this.dataService.getUser().then((data)=>{
            if(data!=null)
            {
                this.savedUser = JSON.parse(data);                
                this.params.user_id = this.savedUser.userid;
  				this.params.sessionid = this.savedUser.sessionid;
            }            
        });
	}
	chooseFile(){
		console.log('Choosefile');
		FileChooser.open().then( (uri) => {
			console.log(uri);
			FilePath.resolveNativePath(uri).then( (filepath) => {
				console.log(uri, filepath);
				this.nativeFilepath = filepath;
				this.filename = filepath.split('/').pop();
				console.log(this.filename);
			})
			.catch((err)=> { this.showToast(err); });
		})
		.catch((e)=>{ this.showToast(e); });
	}
  	submitForm(){
  		if(this.nativeFilepath!=="" && typeof this.nativeFilepath!= "undefined")
  		{
	  		const fileTransfer = new Transfer;
	  		var options:any;  		
	  		options = {
	  			httpMethod:'POST',
	  			fileName:this.filename,
	  			mimeType: 'multipart/form-data',
	  			params: this.params,
	  			chunkedMode: false,
	  			headers:{ Connection: 'close'},
	  		}
	  		fileTransfer.upload(this.nativeFilepath, 'http://35.154.139.104/crm/apis/upload_documents.php', options).then((data)=>
	  		{
	  			console.log(data);
	  			this.showProgressBar = true;
	  			if(data.responseCode==200)
	  			{
	  				this.showToast('File Uploaded.');
	  			}
	  			else{
	  				this.showToast(data.response);
	  			}
	  		},(err)=>{ this.showToast(err); });

	  		fileTransfer.onProgress((progress)=>
	  		{	  			
	  			this.percentage = Math.ceil((progress.loaded/progress.total)*100);
	  			console.log(this.percentage, this.percentage==100);
	  			if(this.percentage == 100)
	  			{
	  				this.showProgressBar = false;	  				
	  				this.nativeFilepath = '';	  				
	  				//this.percentage = 0;
	  				this.showToast('Document Uploaded.');
	  			}
	  		});
  		}
  		else{
  			this.showToast('Please browse file to upload.');
  		}
  	}
  	showToast(value){
		 let toast = this.toastCtrl.create({
			message: value,
			duration: 5000			
		});
		toast.present();
	}
  
}

//FileChooser.open().then( uri => console.log(uri)).catch(e=>{ console.log(e)});
//FilePath.resolveNativePath(path).then( filepath => console.log(filepath)).catch(err=> console.log(err));
