import { Component,  } from '@angular/core';
//import { Validators, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule  } from '@angular/forms';
//import { FormsModule } from '@angular/platform-browser';
import { NavController, NavParams, Events } from 'ionic-angular';
import { StateCityService } from '../../providers/statecity';
import { Data } from '../../providers/dataStore';

//providers: [ StateCityService, Data ]
@Component({
    selector : 'site',    
    templateUrl : 'site.html',    
})
export class Site {
    public localData = [];
    public liveData = [];
    public listOfSites;
    public user:any;
    public gusertype : string = '';
    public gtype : string;
    gcategory:any;
    public stateList:any;
    cityList:any = [];
    listOfCategories: [{id: string, text: string}]; 
    selectedcategory: any = [];
    public categorytoshowindropdown:any;
    //siteform: FormGroup;
    public sites: any = []; 
    public dynamicForm: any;
    public removeSubmit:boolean = false;  
    constructor( public navParams: NavParams, public navCtrl:NavController, public events: Events, /*public formBuilder: FormBuilder,*/ public satecity: StateCityService, public dataService: Data ) {                                    
        
        this.listOfCategories = [{ 'id': '643', 'text': 'Cement'},
                                 { 'id': '648', 'text': 'CP fitings'},
                                 { 'id': '925', 'text': 'Paints'},
                                 { 'id': '647', 'text': 'Sanitary Ware'},
                                 { 'id': '644', 'text': 'Steel'},
                                 { 'id': '646', 'text': 'Stone'},
                                 { 'id': '645', 'text': 'Tiles'},
                                 { 'id': '649', 'text': 'Wood'}];
    }
    //This function will be called everytime you enter the view.
    ionViewWillEnter() {
        console.log('This function will be called everytime you enter the view.');
        this.dataService.getLocal().then((data) => { 
            if(data)
            {                
                this.localData = JSON.parse(data);
                console.log('Local ', this.localData);                
            }            
        });
        this.dataService.getLive().then((data) => { 
            if(data)
            {                
                this.liveData = JSON.parse(data);                
            }            
        });
    }
    ngOnInit()
    {        
        console.log('ngOnInit');
        this.satecity.getData().subscribe(data =>{
            this.stateList = data;            
        });        
        
        if(typeof this.navParams.get('item')!=='undefined')
        {            
            this.user = this.navParams.get('item');
            //this.sites.push(Object.assign({}, this.user.listOfSites[j]));
            console.log(this.user);                        
            this.gusertype = this.user.userType.toLowerCase();        
            this.gtype = this.user.type;             
            //console.log(this.user.listOfSites.length);

            if(typeof this.user.listOfSites !== 'undefined' && this.user.listOfSites.length > 0)
            {                                   
                this.satecity.getData().subscribe(data =>{
                    this.stateList = data;                                
                    for(let j in this.user.listOfSites)
                    {                    

                        for(let i in this.stateList)
                        {            
                            if(this.stateList[i].id == this.user.listOfSites[j].stateid)
                            {                                
                                this.cityList[j] = this.stateList[i].cities;                
                                break;
                            }
                        }  
                    }
                    
                });
                console.log(this.user.listOfSites.length);                
                this.sites = this.user.listOfSites;
                console.log("this.sites : ", this.sites);                
            }
            else
            {
                console.log('Else Condition seller...');
                if(this.gusertype=='seller')
                {
                    this.sites.push({ category:'', site_name:'', stateid:'', cityid:'', address_type:'', address:'', id:'', latitude:'', longitude:'', site_image:'', isDeleted:0, new:true});
                }
                else{
                    this.sites.push({ site_name:'', stateid:'', cityid:'', address_type:'2', address:'', id:'', latitude:'', longitude:'', site_image:'', isDeleted:0, new:true});
                }
            }
            if(this.gusertype=='seller')
            {
                if(this.gtype!='Manufacturer')                
                {
                    for(let i of this.navParams.get('formatCategory'))
                    {                    
                        for( let j of this.listOfCategories)                    
                        {
                            if(i==j.id)
                            {
                                this.selectedcategory.push(j);
                            }
                        }
                    }
                }
                else
                {
                    for( let i of this.listOfCategories)                    
                    {
                        if(this.navParams.get('formatCategory')==i.id)
                        {
                            this.selectedcategory.push(i);
                            break;
                        }
                    }
                }                
            }            
        }
    }    
    
    formatListOfCategories(eventValue, index)
    {         
        console.log(eventValue, index);
        if(typeof eventValue=='object')       
        {
            let format:any= eventValue;
            for(let i in format){
                if(i==='0')
                {                
                    this.sites[index].category = format[i].toString();
                }
                else{                
                    this.sites[index].category = this.sites[index].category+' |##| '+format[i].toString();
                }            
            }
        }
        else{
            this.sites[index].category = eventValue;
        }               
    }
    onStateChange(value, index)
    {          
        console.log(value, index);
        console.log(this.stateList);              
        for(let i in this.stateList)
        {            
            if(this.stateList[i].id == value)
            {
                this.sites[index].stateid = this.stateList[i].id;
                this.sites[index].state = this.stateList[i].statename;
                this.cityList[index] = this.stateList[i].cities;                
                break;
            }
        }
    };
    onCityChange(eventValue, index){        
        for(let i in this.cityList[index]){                        
            if(this.cityList[index][i].id == eventValue)
            {
                this.sites[index].city = this.cityList[index][i].name;
                break;
            }            
        }
    };
    addItem(){                
        console.log('Add Item...'+this.gusertype);
        if(this.gusertype=='buyer')
        {            
            this.sites.push({ site_name:'', stateid:'', cityid:'', address_type:'2', address:'', id:'', latitude:'', longitude:'', site_image:'', isDeleted:0});
        }
        else{            
            this.sites.push({ category:'', site_name:'', stateid:'', cityid:'', address_type:'', address:'', id:'', latitude:'', longitude:'', site_image:'', isDeleted:0});
        }
    }
    searchLocal(data)
    {        
        for(let i in this.localData)
        {            
            if(this.localData[i].localID == data.localID)
            {                
                this.localData[i]= data;
                return true;
            }            
        }
        return false;
    }
    searchLive(data)
    {
        for(let i in this.liveData)
        {
            
            if(this.liveData[i].localID == data.localID && this.liveData[i].exist)
            {
                this.liveData[i]= data;
                return true;
            }            
        }
        return false;
    }
    searchInLocalToRemove(data, index)
    {        
        for(let i in this.localData)
        {            
            if(this.localData[i].localID == data.localID)
            {       
                /*This is to delete a existing site address*/
                if(data.listOfSites[index].id!='')
                {
                    let _sites = data;
                    _sites.isChanged = 1;                    
                    _sites.listOfSites[index].isDeleted = 1;                    
                    this.localData[i].listOfSites = _sites;
                    if(this.liveData[i].localID == this.user.localID)
                    {
                        this.liveData[i].listOfSites = _sites;
                        break;                                        
                    }                     
                    this.sites.splice(index, 1);
                }
                else{
                    let _sites = data;

                    this.sites.splice(index, 1);
                    console.log('sites',this.sites);
                     _sites.listOfSites = this.sites;
                    //_sites.listOfSites = (typeof this.sites!= 'undefined' && this.sites.length>0) ? this.sites : [];
                    this.localData[i].listOfSites = _sites.listOfSites;
                    for(let i in this.liveData)
                    {                
                        if(this.liveData[i].localID == this.user.localID)
                        {
                            this.liveData[i].listOfSites = _sites.listOfSites;
                            break;                                            
                        }            
                    }                                        
                }                        
                return true;
            }            
        }
        return false;
    }
    
    checkUndefined(index){        
        for(let i in this.liveData)
        {
            if(this.liveData[i].localID == this.user.localID)
            {                
                if(typeof this.liveData[i].listOfSites[index]=='undefined'){  return true; }                               
            }            
        }
        return false;
    }
    removeItem(index){                       
        if(this.checkUndefined(index))
        {
            this.sites.splice(index, 1);             
        }
        else{            
            if(this.searchInLocalToRemove(this.user, index))
            {                
                this.dataService.setLocal(this.localData);
                this.dataService.setLive(this.liveData);                
            }
            else{
                let _sites = this.user;
                _sites.isChanged = 1;
                _sites.listOfSites[index].isDeleted = 1;
                this.localData.push(_sites);
                this.dataService.setLocal(this.localData);
                
                for(let i in this.liveData)
                {                
                    if(this.liveData[i].localID == this.user.localID)
                    {                  
                        this.liveData[i].isChanged = 1;                                                 
                        this.liveData[i].listOfSites = _sites.listOfSites;                                                                   
                        break;
                    }            
                }
                this.dataService.setLive(this.liveData);
                this.sites.splice(index, 1);

            }            
        }
        this.removeSubmit = true;
    }
    
    submit(form)
    {    
        
        if(!this.removeSubmit)
        {
            this.user.listOfSites = this.sites;              
            if(this.user.listOfSites.length>0)
            {
                if(this.searchLocal(this.user)){
                    
                    for(let i in this.liveData)
                    {                    
                        if(this.liveData[i].localID == this.user.localID)
                        {                            
                            this.liveData[i]= this.user;                        
                        }            
                    }
                    this.dataService.setLocal(this.localData);
                    this.dataService.setLive(this.liveData);                
                }
                else
                {
                    if(this.searchLive(this.user)){                    
                        this.user.isChanged = 1;
                        this.localData.push(this.user);                        
                        this.dataService.setLocal(this.localData);
                        this.dataService.setLive(this.liveData);                        
                    }
                }
            }
            this.events.publish('reloadListSite');
        }           
        this.navCtrl.pop();
    }
    ionViewWillLeave(){                
        this.events.publish('reloadListSite');        
    }
}




/*this.sites.push(Object.assign({}, this.user.listOfSites[j]));*/