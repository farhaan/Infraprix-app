import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, Events, AlertController, ToastController} from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
//import { Toast } from 'ionic-native';
import { Popover } from '../popover/popover';
import { regForm } from '../regForm/regForm';
import { StateCityService } from '../../providers/statecity';
import { Data } from '../../providers/dataStore';
import { ConnectivityService } from '../../providers/connectivityService';
import { LocationTracker } from '../../providers/locationTracker';

@Component({
    selector : 'regList',
    providers: [ StateCityService, Data ],    
    templateUrl : 'reglist.html'   

})
export class regList{
	
	public list = [];
    public localData = [];
    public liveData = [];
    public liveDataCopy = [];
	stateList:any;
	cities:any;
    savedUser:any = {}; 
    public infiniteScroll:any;   
    public searchText: String='';
    public searchBtn : boolean = false;
    public searchBox : boolean = false;
    constructor( public stc: StateCityService, public dataService: Data, public locationTracker:LocationTracker, public events: Events, public navCtrl:NavController, public storage: Storage, public navParams:NavParams, public popoverCtrl: PopoverController, public alertCtrl: AlertController, public toastCtrl: ToastController, public connectivityservice: ConnectivityService ){

    }
    presentPopover(ev, item){        
        console.log('Item at popover', item);
        let popover = this.popoverCtrl.create(Popover, {item:item});        
        popover.present({
            ev: event
        });
    }

    ngOnInit(){  

    	this.stc.getData().subscribe(data =>{ this.stateList = data; });                            
        this.events.subscribe('reloadList', ()=>{
            console.log('ReloadList');            
        });        
        this.events.subscribe('reloadListSite', ()=>{
            console.log('ReloadListSite'); 
            this.dataService.getLocal().then((data) => { 
                if(data)
                {                
                    this.localData = JSON.parse(data);
                }            
            });           
            this.dataService.getLive().then((data) => { 
                if(data)
                {                
                    this.liveData = JSON.parse(data);
                    this.liveDataCopy = JSON.parse(data);
                    this.list = JSON.parse(data);
                }            
            });
        });
        this.dataService.getUser().then((data)=>{
            if(data!=null)
            {
                this.savedUser = JSON.parse(data);
                console.log(this.savedUser);                                
                this.locationTracker.startTracking(this.savedUser.userid);        
            }            
        });
        
    }
    refreshData(){
        console.log('RefreshData...');
        this.dataService.getLocal().then((data) => { 
            if(data)
            {                
                this.localData = JSON.parse(data);
            }            
        });
        this.dataService.getLive().then((data) => { 
            if(data)
            {                
                this.liveData = JSON.parse(data);
                this.liveDataCopy = JSON.parse(data);
                this.list = JSON.parse(data);
            }            
        });
        this.dataService.getUser().then((data)=>{
            if(data!=null)
            {
                this.savedUser = JSON.parse(data);                
            }            
        });                
    }    
    //This function will be called everytime you enter the view.
    ionViewWillEnter() {
        console.log('This function will be called everytime you enter the view.');                               
        this.refreshData();         
    }    
    doRefresh(refresher) {                       
        if(this.connectivityservice.isOnline())
        {                    
            if(this.localData.length>0)
            {            
                this.showAlert('Upload', 'Upload your data first');                                   
            }
            else{
                this.refreshList();
            }
        }
        else
        {
            this.showToast('failed to connect network');
        }
        refresher.complete();                               
        
    }
    searchFn(){        
        //console.log(this.liveData.filter((item) => { return item.companyName.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1; }));
        this.list = this.liveData.filter((item) => { return item.companyName.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1; });
    }
    addItem(){
      this.navCtrl.push(regForm);
    }    
    getCity(value)
    {                
        for(let i in this.stateList)
        {            
            if(this.stateList[i].id == value)
            {                
                this.cities = this.stateList[i].cities;
                return true;                                
            }
        }
        return false;        
    };        
    showDetail(item){        
        let categories;
        item.userType = item.userType.toLowerCase();
        if(item.userType == 'seller'){
            if(item.type!='Manufacturer')
            {
                if(item.categories!=null)
                categories = item.categories.split(' |##| ');    
            }
            else{
                categories = item.categories;
            }                        
        }        
    	if(this.getCity(item.stateId))
    	{
    		this.navCtrl.push(regForm, { item: item, cities:this.cities, formatCategory:categories});
    	}   
    }
    searchItemInLocaltoDelete(item){        
        for(let i in this.localData)
        {            
            if(this.localData[i].localID == item.localID)
            {                   
                this.localData.splice(Number(i), 1);                
                return true;                
            }
            return false;
        }
    }
    searchItemInLivetoDelete(item){
        console.log(this.liveData);
        for(let j in this.liveData)
        {                
            if(this.liveData[j].localID == item.localID)
            {                                
                if( typeof this.liveData[j].exist!== 'undefined' && this.liveData[j].exist)
                {
                    console.log('Its existing data push in local.');
                    this.liveData[j].isDeleted = 1;
                    this.liveData[j].isChanged = 1;
                    this.localData.push(this.liveData[j]); 
                }
                this.liveData.splice(Number(j), 1);                
                return true                                       
            }                
        }
        return false;
    }    
    
    removeItem(item, index)
    {
        console.log(item);
        let confirm = this.alertCtrl.create({
            title: 'Delete',
            message: 'Are you sure want to delete this record.',
            buttons:[
                    {   text : 'Cancel',
                        handler:() => {
                            console.log('Cancel...');                            
                        }
                    },
                    {
                        text:'Delete',
                        handler:() => {
                            console.log('Delete');                            
                            if(this.searchItemInLocaltoDelete(item))
                            {                                
                                for(let j in this.liveData)
                                {
                                    if(this.liveData[j].localID == item.localID)
                                    {                                        
                                        this.liveData.splice(Number(j), 1);
                                        break;                                       
                                    }                                    
                                }
                                this.dataService.setLocal(this.localData);
                                this.dataService.setLive(this.liveData);
                                this.list.splice(index, 1);            
                            }
                            else
                            {
                                if(this.searchItemInLivetoDelete(item))            
                                {                                                                  
                                    this.dataService.setLocal(this.localData);
                                    this.dataService.setLive(this.liveData);                        
                                    this.list.splice(index, 1);                                    
                                }
                            }
                        }
                    }
                ]
            });
        confirm.present();               
    }
    makeid()
    {
       var text = "";
       var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

       for( let i=0; i < 5; i++ )
           text += possible.charAt(Math.floor(Math.random() * possible.length));

       return text;
    }
    refreshList(){
        console.log(this.savedUser.userid);
        this.dataService.getList('0', this.savedUser.userid).subscribe(
            data =>{                                 
                if(data.statusCode == '200')
                {
                    this.list = [];                      
                    for(let i of data.data)        
                    {   
                        i.exist = true;          
                        i.localID = this.makeid();                        
                        this.list.push(i);                    
                    }                                       
                    
                    this.showToast('Record refreshed.');
                    if(typeof this.infiniteScroll!== 'undefined')
                    this.infiniteScroll.enable(true);
                }
                else if(data.statusCode=='301')
                {     
                    this.showAlert(data.statusCode, data.error);                                   
                }
                else if(data.statusCode=='300'){
                    this.showAlert(data.statusCode, data.error);                                                       
                }
                else{  
                    this.showAlert('Error', 'server error');                                                       
                                                                              
                }                              
            },
            error =>{ this.showToast(error); },
            () => {                 
                this.dataService.setLive(this.list); 
                this.navCtrl.setRoot(this.navCtrl.getActive().component);                                
            }
        );
    }
    doInfinite(infiniteScroll){        
        this.infiniteScroll = infiniteScroll;        
        if(this.connectivityservice.isOnline())
        {                       
            this.dataService.getList(this.list.length, this.savedUser.userid).subscribe(
                data =>{                    
                    if(data.statusCode == '200')
                    {                        
                        for(let i of data.data)        
                        {   
                            i.exist = true;          
                            i.localID = this.makeid();                        
                            this.list.push(i);                    
                        }                        
                        this.dataService.setLive(this.list);
                        infiniteScroll.complete();                         
                    }
                    else if(data.statusCode=='300'){ this.showAlert(data.statusCode, data.error); }
                    else if(data.statusCode=='301'){ this.showAlert(data.statusCode, data.error); }
                    else if(data.statusCode=='304'){ infiniteScroll.enable(false); this.showToast(data.error); }
                    else{  this.showAlert('Error', 'server error'); }                    
                }, error=>{ this.showToast(error); infiniteScroll.complete();},
                () => { infiniteScroll.complete(); }
            );
        }
        else{
            this.showToast('failed to connect network');
            infiniteScroll.complete();
        }
        
    }
    showAlert(title, msg){
        let alert = this.alertCtrl.create({
            title: title,
            message: msg,
            buttons: ['Dismiss']
        });
        alert.present();
    }
    showToast(msg){
         let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000            
        });
        toast.present();
    }
    searchItem(){
        this.searchText = '';
        this.list = this.liveData;

        this.searchBox = (this.searchBox) ? false : true;
    }
}