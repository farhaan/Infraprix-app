import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Data } from '../../providers/dataStore';

@Component({
    selector : 'settings',
    templateUrl : 'settings.html',    
})
export class Settings {
	public enabletracking: boolean;
	locationItem:any;
	constructor( public navCtrl: NavController, public navParams: NavParams, public dataService: Data, public alertCtrl: AlertController, public toastCtrl: ToastController){		
	}
		
	clearData(){
		
		let confirm = this.alertCtrl.create({
            title: 'Delete',
            message: 'Are you sure want to delete this record.',
            buttons:[
                    {   text : 'Cancel',
                        handler:() => {
                            console.log('Cancel...');                            
                        }
                    },
                    {
                        text:'Delete',
                        handler:() => {
                            console.log('clearData');
							this.dataService.setLive([]);
							this.dataService.setLocal([]);
							this.showToast('Data Deleted...');
                        }
                    }
                ]
            });
        confirm.present();
	}
    showToast(msg){
         let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000            
        });
        toast.present();
    }

}
/*BackgroundGeolocation.getValidLocations().then((locations)=>{			
			console.log(locations);
			let lastTimeStamp = locations.pop();
            let date = new Date(lastTimeStamp.time);
            
            console.log(date.toUTCString());
            console.log(date.toLocaleString());
            BackgroundGeolocation.deleteAllLocations().then((res)=>{ console.log(res); });		
		})*/

/*BackgroundGeolocation.configure((location) => {
    console.log('[js] BackgroundGeolocation callback:  ' + location.latitude + ',' + location.longitude);
    Toast.show(location.latitude+" : "+location.longitude, '5000', 'center').subscribe(
	  toast => {
	    console.log(toast);
	  }
	);
    BackgroundGeolocation.finish(); // FOR IOS ONLY

 }, (error) => {
   console.log('BackgroundGeolocation error');
 }, config);

// Turn ON the background-geolocation system.  The user will be tracked whenever they suspend the app.
BackgroundGeolocation.start();
BackgroundGeolocation.stop();*/
